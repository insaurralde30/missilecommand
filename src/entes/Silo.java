package entes;

public class Silo extends Entidad {
	final private int cantMisiles;
	private int puntajeMisil;
	private int cantMisActual;
	
	public Silo(double x, double y) {
		//seteo cant de misiles por silo y ubicacion del silo
		super(x,y);
		this.cantMisiles=3;
		this.cantMisActual=this.cantMisiles;
	}
	
	public void isDestruido() {
		super.isDestruida();
		System.out.println("Silo" + this.getClass().getSimpleName() + " ha sido destruido!");
	}
	
	public int Puntaje(int puntajeXMisil) {
		return cantMisActual*puntajeXMisil;
	}
	
}
